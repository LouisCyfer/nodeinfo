local version = minetest.get_current_modname() .. " v0.6.2b"

player_identifier_table = {}
local clearImg = "bg.png"
local cornerPos = {x=0.77,y=0.825}
local linesize = 18
local timer = 0
local cTime = "00:00:00"

local use_debug = false
player_debug = {}

local found_mods = {
	basic_machines = false,
	pipeworks = false,
	drawers = false,
	mobs = false,
	flowerpot = false,
	teleporters = false,
	ethereal = false,
	ironage = false,
	aliveai = false
}

function applyModSupport()
	for i, name in ipairs(minetest.get_modnames()) do
		if name == "basic_machines" then found_mods.basic_machines = true
		elseif name == "pipeworks" then found_mods.pipeworks = true
		elseif name == "drawers" then found_mods.drawers = true
		elseif name == "mobs" then found_mods.mobs = true
		elseif name == "flowerpot" then found_mods.flowerpot = true
		elseif name == "teleporters" then found_mods.teleporters = true
		elseif name == "ethereal" then found_mods.ethereal = true
		elseif name == "moretrees" then found_mods.moretrees = true
		elseif name == "ironage" then found_mods.ironage = true
		elseif name == "ironage" then found_mods.aliveai = true
		end
	end
end
applyModSupport()

function getTimeString()
	local timeofday
	local name
	local twentyfour
	
	local minutes
	local hour
	
	local timestring
	local meridiem
	
	meridiem  = " AM"
	timeofday = minetest.get_timeofday()
	
	--convert 24000 minutes into 24 hours
	hour    = math.floor(((timeofday) % 24000)*24)
	minutes = math.floor(((math.floor(((timeofday) % 24000)*2400)-(hour*100))/50)*30)
	--don't do seconds for cpu sake
	
	--turn the raw time into easy to view 12 hour meridiem
	if hour >= 12 then
		meridiem = " PM"
	end
	if hour > 12 then
		hour = hour - 12
	end
	timestring = tostring(hour)
	if string.len(minutes) == 1 then
		timestring = timestring..":".."0"..minutes
	else
		timestring = timestring..":"..minutes
	end
	timestring = timestring..meridiem
	return timestring
end

function splittedString(inputStr)

	local reString = ""
	local maxLineLen = 55

	if inputStr == nil or type(inputStr) ~= "string" then
		return "n/a"
	end

	local splitIndex = 0
	local strLen = string.len(inputStr)
	local maxLines = math.ceil(string.len(inputStr)/maxLineLen)	
	local thisline = ""
	
	for lineCount = 1, maxLines do

		if lineCount == maxLines then
			thisline = inputStr
		else
			local tmpIndex = string.find(string.reverse(string.sub(inputStr, 0, maxLineLen)), " ")
			-- dbg = "tmpIndex=" .. tostring(tmpIndex)

			splitIndex = string.find(inputStr, " ", maxLineLen - tmpIndex, maxLineLen)
	
			thisline = string.sub(inputStr, 0, splitIndex)
			thisline = thisline .. "\n"
			inputStr = string.sub(inputStr, splitIndex + 1)
		end

		reString = reString .. thisline
	end
	return reString
end

function fillImage(node_name)
	-- minetest.log("action", "DEBUG: " .. dump(node_name))
	-- minetest.log("action", "DEBUG: " .. dump(minetest.registered_nodes[node_name]))

	if minetest.registered_items[node_name] == nil or node_name == nil then
		return minetest.inventorycube(clearImg, clearImg, clearImg)
	end

	local inv_img = minetest.registered_items[node_name].inventory_image
	local tiles = minetest.registered_nodes[node_name].tiles
	local modname = minetest.registered_items[node_name].mod_origin

	-- minetest.log("action", "DEBUG: type(tiles[max_tiles])=table")

	if found_mods.aliveai == true then

		-- DEV NOTES:
		-- currently have to block all nodes from [aliveai] hope this helps for now because it is not really testable for me
		-- https://forum.minetest.net/viewtopic.php?f=11&t=16083&hilit=aliveai

		aiNames= { "aliveai", "aliveai_aliens", "aliveai_ants", "aliveai_chemistry", "aliveai_electric",
		"aliveai_folk", "aliveai_massdestruction", "aliveai_minecontroller", "aliveai_nitroglycerine",
		"aliveai_pyramid", "aliveai_storm", "aliveai_threat_electric", "aliveai_threats"}

		ainodeFound = false

		for idx, name in pairs(aiNames) do
			if node == modname then
				ainodeFound = true
				break
			end
		end

		if ainodeFound == true then return clearImg end
	end	

	-- minetest.log("action", "DEBUG: inv_img\n" .. dump(inv_img))
	-- minetest.log("action", "DEBUG: tiles\n" .. dump(tiles))
	-- minetest.log("action", "DEBUG: tiles-type=" .. type(tiles))

	if type(tiles) == "table" then

		local max_tiles = 1
		local cube_tiles = { tile1 = clearImg, tile2 = clearImg, tile3 = clearImg }

		if minetest.registered_nodes[node_name].tiles then
			tiles = minetest.registered_nodes[node_name].tiles
			max_tiles = #tiles
		end

		if type(tiles[max_tiles]) == "table" then

			if tiles[1].name then
				local name_tile = tiles[1].name
				cube_tiles = { tile1 = name_tile, tile2 = name_tile, tile3 = name_tile }
			
				if found_mods.flowerpot == true and modname == "flowerpot" then
					-- minetest.log("action", "DEBUG: flowerpot tiles\n" .. dump(tiles))
					-- return tiles[1]
					cube_tiles.tile1 = clearImg
					-- cube_tiles.tile2 = tiles[1]
					-- cube_tiles.tile3 = tiles[1]
				end

				if node_name == "default:torch" then return inv_img
				elseif node_name == "default:torch_wall" then return name_tile

				end
			else
				cube_tiles.tile1 = tiles[1]

				if tiles[max_tiles].name then
					cube_tiles.tile2 = tiles[max_tiles].name
					cube_tiles.tile3 = cube_tiles.tile2
				end
			end

			return minetest.inventorycube(cube_tiles.tile1, cube_tiles.tile2, cube_tiles.tile3)

		elseif type(tiles[max_tiles]) == "string" then
			-- minetest.log("action", "DEBUG: type(tiles[max_tiles])=string")

			cube_tiles.tile1 = tiles[max_tiles]
			cube_tiles.tile2 = tiles[max_tiles]
			cube_tiles.tile3 = tiles[max_tiles]

			if max_tiles > 1 then
				cube_tiles.tile1 = tiles[1]
				cube_tiles.tile3 = tiles[max_tiles-1]
			end

			local no_cube = false

			-- minetest.log("action", "DEBUG: inv_img=" .. dump(inv_img))
			-- if inv_img ~= "" then no_cube = true end
			if inv_img ~= "" then return inv_img end

			if modname ~= "default" then
				
				if modname == "bones" and node_name == "bones:bones" then
					cube_tiles.tile1 = tiles[1].name
					cube_tiles.tile2 = tiles[2]
					cube_tiles.tile3 = tiles[3]

				elseif modname == "mesecons_wires" then
					return inv_img

				--[[
				elseif found_mods.pipeworks == true and modname == "pipeworks" then

					local no_inv_image = false
					if node_name == "pipeworks:autocrafter" then
					else
						return inv_img
					end
				]]--

				elseif found_mods.moretrees == true and modname == "moretrees" then
					if node_name == "default:apple" then no_cube = true end

				elseif found_mods.ethereal == true and modname == "ethereal" then
					cube_tiles.tile1 = tiles[1]
					cube_tiles.tile2 = tiles[3]
					cube_tiles.tile3 = tiles[3]

				elseif found_mods.teleporters == true and modname == "teleporters" then

					if node_name == "teleporters:unlinked" then cube_tiles.tile1 = tiles[1]
					else cube_tiles.tile1 = tiles[1].name end

					cube_tiles.tile2 = tiles[3]
					cube_tiles.tile3 = tiles[3]					
					
				elseif found_mods.basic_machines == true and modname == "basic_machines" then
					cube_tiles.tile1 = tiles[1]
					cube_tiles.tile2 = tiles[3]
					cube_tiles.tile3 = tiles[3]

				elseif found_mods.ironage == true and modname == "ironage" then

					if node_name == "ironage:meltingpot" or "ironage:meltingpot_active" then
						return minetest.inventorycube(clearImg, clearImg, clearImg)
					end

					cube_tiles.tile1 = tiles[1]
					cube_tiles.tile2 = tiles[1]
					cube_tiles.tile3 = tiles[1]

				else
					return minetest.inventorycube(clearImg, clearImg, clearImg)
				end

			else
				if node_name == "default:water_flowing" then
					no_cube = false
				elseif node_name == "default:apple" and found_mods.moretrees == false then
					no_cube = true
				end
			end

			-- minetest.log("action", "DEBUG: no_cube=" .. dump(no_cube))
			if no_cube == true then

				if max_tiles == 1 then
					if tiles[max_tiles] == inv_img then
						return inv_img
					else
						return tiles[max_tiles]
					end
				else
					return minetest.inventorycube(clearImg, clearImg, clearImg)
				end
			end

			return minetest.inventorycube(cube_tiles.tile1, cube_tiles.tile2, cube_tiles.tile3)
			
		else
			-- minetest.log("action", "DEBUG: type(tiles[max_tiles])=" .. type(tiles[max_tiles]))
			return minetest.inventorycube(clearImg, clearImg, clearImg)
		end
	end

	-- fallback method
	if type(inv_img) == "string" and string.len(inv_img) > 0 then
		-- minetest.log("action", "DEBUG: returning inv_img")
		return inv_img
	end

	minetest.log("action", "DEBUG: returning clearImg")
	return clearImg
end

minetest.register_on_joinplayer(function(player)

	local p_name = player:get_player_name()

	if player_identifier_table[p_name] == nil then
		player_identifier_table[p_name] = {}	
	end

	player_identifier_table[p_name].name = "air"
	player_identifier_table[p_name].amount = 1

	if use_debug == true then
		if player_debug[p_name] == nil then
			player_debug[p_name] = "n/a"
		end

		player_identifier_table[p_name].dbg = player_debug[p_name]
		player_identifier_table[p_name].huddebug = player:hud_add({
			hud_elem_type = "text",
			position = {x=0,y=0},
			size = "",
			text = "DEBUG:",
			number = 0x00FF00,
			alignment = {x=1,y=1},
			offset = {x=0, y=0}
		})
	end

	player_identifier_table[p_name].hud_bg = player:hud_add({
		hud_elem_type = "image",
		position = cornerPos,
		size = "",
		text = "bg.png",
		number = 20,
		alignment = {x=1,y=1},
		offset = {x=0, y=0},
		scale = {x=40,y=20}
	})

	player_identifier_table[p_name].hud_image = player:hud_add({
		hud_elem_type = "statbar",
		position = cornerPos,
		size = {x=50,y=50},
		text = "bg.png",
		number = 2,
		offset = {x=linesize, y=25}
	})

	player_identifier_table[p_name].hud_titlebar = player:hud_add({
		hud_elem_type = "text",
		position = cornerPos,
		size = "",
		text = "Block-Info:",
		number = 0x00FF00,
		alignment = {x=1,y=1},
		offset = {x=5, y=0}
	})

	player_identifier_table[p_name].hud_title = player:hud_add({
		hud_elem_type = "text",
		position = cornerPos,
		size = "",
		text = "air",
		number = 0xFFFFFF,
		alignment = {x=1,y=1},
		offset = {x=82, y=0}
	})

	player_identifier_table[p_name].hud_modinfo = player:hud_add({
		hud_elem_type = "text",
		position = cornerPos,
		size = "",
		text = " ",
		number = 0xFFFFFF,
		alignment = {x=1,y=1},
		offset = {x=82, y=linesize*2}
	})

	player_identifier_table[p_name].hud_description = player:hud_add({
		hud_elem_type = "text",
		position = cornerPos,
		size = "",
		text = " ",
		number = 0xFFFFFF,
		alignment = {x=1,y=1},
		offset = {x=5, y=linesize*5}
	})

	player_identifier_table[p_name].hudtext = player:hud_add({
		hud_elem_type = "text",
		position = {x=0.72,y=0.927},
		size = "",
		text = " ",
		number = 0xFFFFFF,
		alignment = {x=1,y=1},
		offset = {x=0, y=0}
	})

	player_identifier_table[p_name].clocktext = player:hud_add({
		hud_elem_type = "text",
		position = {x=0.993,y=0.993},
		size = "",
		text = cTime,
		number = 0xFFFFFF,
		alignment = {x=-1,y=-1},
		offset = {x=0, y=0}
	})
end)

minetest.register_globalstep(function(dtime)
	timer = timer + dtime

	if timer >= 0.1 then

		cTime = getTimeString()
		if cTime == nil then
			cTime = "00:00:00"
		end
		timer = 0
	else
		return
	end

	for _,player in ipairs(minetest.get_connected_players()) do
		--Find some better way to do this

		-- if use_debug == true then
			-- player_debug[p_name] = player_identifier_table[p_name].dbg
		-- end

		local p_name = player:get_player_name()
		player:hud_change(player_identifier_table[p_name].clocktext, "text", cTime)

		local look
		local pos
		local distance
		local newdir
		local newlook

		local node = nil
		local oldnode
		local oldamount
		local set_node_name = "air"

		local text_title = "air"
		local text_modInfo = ""
		local text_description = ""

		local setImg = minetest.inventorycube(clearImg, clearImg, clearImg)
		local tiles

		oldnode = player_identifier_table[p_name].name
		oldamount = player_identifier_table[p_name].amount

		newdir   = {}
		newlook  = {}
		distance = 3
		look     = player:get_look_dir()
		pos      = player:getpos()
		pos.y = pos.y + 1.5

		local nodeInfos = ""
		local entityInfos = ""

		local foundEntity = {found=false, isPlayer=false, name=nil}

		-- get closest node/entity that is not air/water
		for i = 1, 4 do
			newlook.x = look.x*i
			newlook.y = look.y*i
			newlook.z = look.z*i
			
			newdir.x = (newlook.x) + pos.x
			newdir.y = (newlook.y) + pos.y
			newdir.z = (newlook.z) + pos.z
			
			node = minetest.get_node(newdir)
			local entList = minetest.get_objects_inside_radius(newdir, 0.5)
			foundEntity = {
				texture = setImg,
				found = false,
				entity = nil,
				isPlayer = false,
				isMob = false,
				name = nil,
				orgName = "",
				amount = 1,
				maxAmount = 1,
				hp = 0,
				health = 0,
				follow = {"n/a"},
				owner = "n/a",
				tamed = false,
				protected = false
			}
			
			local _,object
			for _,object in ipairs(entList) do
				foundEntity.isPlayer = object:is_player()
				foundEntity.hp = object:get_hp()
				foundEntity.found = true

				if object:is_player() then
					foundEntity.name = object:get_player_name()
					break
				else
					if object:get_luaentity() then
						local ent = object:get_luaentity()
						foundEntity.entity = ent

						if ent.name == "__builtin:item" then
							local entStr = string.split(ent.itemstring, " ")
							foundEntity.name = entStr[1]
							if #entStr > 1 then
								foundEntity.amount = entStr[2]
								foundEntity.maxAmount = foundEntity.amount
							end
							break
						elseif ent.name == "drawers:visual" and found_mods.drawers == true then
							foundEntity.texture = ent.texture
							foundEntity.orgName = ent.name

							if ent.itemName ~= nil then
								foundEntity.name = ent.itemName
								foundEntity.amount = ent.count
								foundEntity.maxAmount = ent.maxCount
							end

							if foundEntity.name == "" then foundEntity.name = ent.name end

							break
						else
							if found_mods.mobs == true then
								foundEntity.isMob = true
								foundEntity.name = ent.name
								if ent.follow then foundEntity.follow = ent.follow end
								if ent.owner then foundEntity.owner = ent.owner end
								if ent.tamed then foundEntity.tamed = ent.tamed end
								if ent.protected then foundEntity.protected = ent.protected end
								if ent.health then foundEntity.health = ent.health end
								break
							end
						end
					end
				end
			end

			nodeInfos = nodeInfos .. tostring(i) .. "=" .. node.name .. "\n"
			entityInfos = entityInfos .. tostring(i) .. "\nfoundEntity:\n" .. dump(foundEntity) .. "\n---\n"
			-- player_debug[p_name] = entityInfos

			if foundEntity.found == true then set_node_name = foundEntity.name break
			else
				if node.name ~= "air" and node.name ~= "ignore" then
					if node.name == "default:water_source" or node.name == "default:water_flowing" then
						if i == 2 then break end
					else break end
				end
			end
		end

		if node ~= nil and foundEntity.found == false then
			set_node_name = node.name
		end

		if node ~= nil and set_node_name ~= nil and set_node_name ~= "air" and node.name ~= "ignore" then

			setImg = player:hud_get(player_identifier_table[p_name].hud_image)["text"]
			text_title = player:hud_get(player_identifier_table[p_name].hud_title)["text"]
			text_modInfo = player:hud_get(player_identifier_table[p_name].hud_modinfo)["text"]
			text_description = player:hud_get(player_identifier_table[p_name].hud_description)["text"]

			if set_node_name ~= oldnode or foundEntity.amount ~= oldamount then
				player_identifier_table[p_name].name = set_node_name
				player_identifier_table[p_name].amount = foundEntity.amount

				if foundEntity.found == true then setImg = foundEntity.texture
				else
					if set_node_name then
						setImg = fillImage(set_node_name)

						-- if tmpImg then setImg = tmpImg end
					end
				end

				player:hud_change(player_identifier_table[p_name].hud_image, "text", setImg)

				local reciepesTable = minetest.get_all_craft_recipes(set_node_name)
				local recAmount = 0

				if reciepesTable ~= nil then recAmount = #reciepesTable end

				local getStr = set_node_name

				if use_debug == true then
					-- player_debug[p_name] = player_debug[p_name] .. "\n\nfoundEntity=\n" .. dump(foundEntity) .. "\n"
					player_debug[p_name] = "set_node_name=" .. set_node_name
					-- player_debug[p_name] = player_debug[p_name] .. "\n\n" .. dump(minetest.registered_entities[foundEntity.orgName])
				end

				if minetest.registered_items[set_node_name] ~= nil then
					getStr = minetest.registered_items[set_node_name].description

					if player_debug[p_name] ~= nil then
						player_debug[p_name] = player_debug[p_name] .. "\ninventory_image"
						if minetest.registered_items[set_node_name].inventory_image ~= nil then
							player_debug[p_name] = player_debug[p_name] .. " (" .. type(minetest.registered_items[set_node_name].inventory_image) .. ")"
						end
					end

					-- player_debug[p_name] = player_debug[p_name] .. "\ninventory_image (" .. type(minetest.registered_items[set_node_name].inventory_image) .. ")=" .. dump(minetest.registered_items[set_node_name].inventory_image)

					if minetest.registered_items[set_node_name].tiles then
						local max_tiles = #minetest.registered_items[set_node_name].tiles
						if use_debug == true then
							player_debug[p_name] = player_debug[p_name] .. "\n" .. "max_tiles=" .. max_tiles .. " (" .. type(minetest.registered_items[set_node_name].tiles[max_tiles]) .. ")"
							player_debug[p_name] = player_debug[p_name] .. "\ntiles=" .. dump(minetest.registered_items[set_node_name].tiles)
						end
					end
				end

				text_title = getStr
				text_description = " "

				local checkStr = " "

				if found_mods.pipeworks == true then
					checkStr = " {"
					if string.find(getStr, checkStr) then
						getStr = string.split(getStr, checkStr)[1]
						-- text_title = string.split(getStr, checkStr)[1]
						-- text_description = string.split(getStr, checkStr)[2]
					end
				end

				if found_mods.basic_machines == true then
					checkStr = " - "

					if string.find(getStr, checkStr) then
						text_title = string.split(getStr, checkStr)[1]
						text_description = string.split(getStr, checkStr)[2]
					end
				end

				checkStr = ": "

				if string.find(getStr, checkStr) then
					text_title = string.split(getStr, checkStr)[1]
					text_description = string.split(getStr, checkStr)[2]
				end

				text_description = splittedString(text_description)

				if foundEntity.found == true then
					text_title = text_title .. " x" .. tostring(foundEntity.amount)

					if foundEntity.isMob == true then
						text_description = "HP: " .. tostring(foundEntity.health) .. "/" .. tostring(foundEntity.hp)
						text_description = text_description .. " | tamed: " .. tostring(foundEntity.tamed)  .. " | protected: " .. tostring(foundEntity.protected) .. "\n"
						text_description = text_description .. "Owner: " .. foundEntity.owner .. "\n"

						local followStr= ""
						if type(foundEntity.follow) == "string" then
							followStr = foundEntity.follow
						else
							followStr = table.concat(foundEntity.follow, ", ")
						end

						text_description = text_description .. "follow: " .. splittedString(followStr)

					elseif foundEntity.orgName == "drawers:visual" and found_mods.drawers == true then
						local getPercent = math.floor(((foundEntity.amount / foundEntity.maxAmount) * 100) + 0.5)

						text_description = "[drawer-info] " .. foundEntity.amount .. "/" .. foundEntity.maxAmount .. " (" .. getPercent .. "% filled)" .. "\n\n" .. text_description
					end
					-- player_debug[p_name] = player_debug[p_name] .. "\nfoundEntity:\n" .. dump(foundEntity.entity)
				end
				-- player_debug[p_name] = player_debug[p_name] .. "\nfoundEntity:\n" .. dump(foundEntity)

				text_title = text_title .. " | Reciepes: " .. tostring(recAmount)
			end
		end

		if set_node_name ~= "air"then
			local setMod = "n/a"

			if minetest.registered_items[set_node_name] ~= nil then
				setMod = minetest.registered_items[set_node_name].mod_origin
			end

			if minetest.registered_entities[set_node_name] ~= nil then
				if found_mods.drawers == true and set_node_name == "drawers:visual" then
					setMod = minetest.registered_entities[set_node_name].mod_origin
				end
			end

			if set_node_name then
				text_modInfo = "Mod: " .. setMod .. "\n --> " .. set_node_name
			end
		end

		player_identifier_table[p_name].name = set_node_name

		player:hud_change(player_identifier_table[p_name].hud_image, "text", setImg)
		player:hud_change(player_identifier_table[p_name].hud_title, "text", text_title)
		player:hud_change(player_identifier_table[p_name].hud_modinfo, "text", text_modInfo)
		player:hud_change(player_identifier_table[p_name].hud_description, "text", text_description)
		
		if use_debug == true then
			if player_debug[p_name] == nil then player_debug[p_name] = "n/a" end

			player_identifier_table[p_name].dbg = player_debug[p_name]
			player:hud_change(player_identifier_table[p_name].huddebug, "text", "DEBUG:\n" .. player_debug[p_name])
		end
	end
end)

if minetest.settings:get("log_mods") then
	minetest.log("action", "[Mod] " .. version .. " loaded")
end